package org.budo.warehouse.logic.consumer;

import org.budo.warehouse.logic.api.AbstractDataConsumer;
import org.budo.warehouse.logic.api.DataMessage;

/**
 * @author lmw
 */
public class NullDataConsumer extends AbstractDataConsumer {
    @Override
    public void consume(DataMessage dataMessage) {
    }
}