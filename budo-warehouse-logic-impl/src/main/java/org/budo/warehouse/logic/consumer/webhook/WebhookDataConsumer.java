package org.budo.warehouse.logic.consumer.webhook;

import org.budo.graph.annotation.SpringGraph;
import org.budo.warehouse.logic.api.AbstractDataConsumer;
import org.budo.warehouse.logic.api.DataMessage;

/**
 * @author limingwei
 */
public class WebhookDataConsumer extends AbstractDataConsumer {
    @SpringGraph
    @Override
    public void consume(DataMessage dataMessage) {
        super.consume(dataMessage); // HTTP POST
    }
}