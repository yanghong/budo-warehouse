package org.budo.warehouse.logic.consumer.async;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.budo.dubbo.protocol.async.repository.BudoAsyncInvocation;
import org.budo.support.lang.util.MapUtil;
import org.budo.warehouse.logic.api.DataEntry;
import org.budo.warehouse.logic.api.DataMessage;

import lombok.extern.slf4j.Slf4j;

/**
 * 非标准版本消息发送
 * 
 * @author lmw
 */
@Slf4j
public class AsyncMessageUtil {
    private static final String EVENT_TYPE = "$eventType";

    public static BudoAsyncInvocation invocation_v1(DataMessage dataMessage) {
        List<Map<String, Object>> v1_message = to_v1_message(dataMessage);

        log.debug("#26 v1_message=" + v1_message);

        BudoAsyncInvocation budoAsyncInvocation = new BudoAsyncInvocation().setMethodName("pushDataToWarehouse") //
                .setParameterTypes(new Class<?>[] { List.class }) //
                .setArguments(new Object[] { v1_message }) //
                .setCreatedAt(System.currentTimeMillis()) // 消息创建时间
                .setId(v1_message.hashCode());
        return budoAsyncInvocation;
    }

    private static List<Map<String, Object>> to_v1_message(DataMessage dataMessage) {
        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
        List<DataEntry> dataEntries = dataMessage.getDataEntries();
        for (DataEntry dataEntry : dataEntries) {
            Integer rowCount = dataEntry.getRowCount();
            for (int rowIndex = 0; rowIndex < rowCount; rowIndex++) {
                Map<String, Object> v1_row = to_v1_row(dataEntry, rowIndex);

                mapList.add(v1_row);
            }
        }

        return mapList;
    }

    public static Map<String, Object> to_qc_row(DataEntry dataEntry, int rowIndex) {
        Map<String, Object> rowMap = MapUtil.stringObjectMap();

        Integer columnCount = dataEntry.getColumnCount(rowIndex);
        for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
            String columnName = dataEntry.getColumnName(rowIndex, columnIndex);
            String columnValue = dataEntry.getColumnValueAfter(rowIndex, columnIndex);
            rowMap.put(columnName, columnValue);
        }

        return rowMap;
    }

    private static Map<String, Object> to_v1_row(DataEntry dataEntry, int rowIndex) {
        String eventType = dataEntry.getEventType();
        Map<String, Object> rowMap = MapUtil.stringObjectMap(EVENT_TYPE, eventType);

        Integer columnCount = dataEntry.getColumnCount(rowIndex);
        for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
            String columnName = dataEntry.getColumnName(rowIndex, columnIndex);

            String columnValue;
            if ("delete".equalsIgnoreCase(eventType)) {
                columnValue = dataEntry.getColumnValueBefore(rowIndex, columnIndex);
            } else {
                columnValue = dataEntry.getColumnValueAfter(rowIndex, columnIndex);
            }

            rowMap.put(columnName, null == columnValue ? "" : columnValue); // for兼容
        }
        return rowMap;
    }
}