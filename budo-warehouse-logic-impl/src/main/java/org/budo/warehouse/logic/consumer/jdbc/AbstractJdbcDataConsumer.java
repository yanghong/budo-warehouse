package org.budo.warehouse.logic.consumer.jdbc;

import java.util.ArrayList;
import java.util.List;

import org.budo.graph.annotation.SpringGraph;
import org.budo.support.lang.util.StringUtil;
import org.budo.warehouse.logic.api.AbstractDataConsumer;
import org.budo.warehouse.logic.api.DataEntry;
import org.budo.warehouse.logic.api.DataEntry.EventType;
import org.budo.warehouse.logic.api.DataMessage;
import org.budo.warehouse.logic.util.DataEntryUtil;
import org.budo.warehouse.logic.util.PipelineUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * @author limingwei
 */
@Slf4j
public abstract class AbstractJdbcDataConsumer extends AbstractDataConsumer {
    /**
     * @see com.alibaba.otter.canal.protocol.CanalEntry.EventType
     */
    @SpringGraph
    public List<SqlUnit> buildSql(DataMessage dataMessage) {
        List<SqlUnit> sqlUnits = new ArrayList<SqlUnit>();

        List<DataEntry> dataEntries = dataMessage.getDataEntries();
        for (DataEntry dataEntry : dataEntries) {
            String eventType = dataEntry.getEventType();

            if (EventType.INSERT.equalsIgnoreCase(eventType)) {
                List<SqlUnit> insertSql = this.insertSql(dataEntry);
                sqlUnits.addAll(insertSql);
                continue;
            }

            if (EventType.DELETE.equalsIgnoreCase(eventType)) {
                List<SqlUnit> deleteSql = this.deleteSql(dataEntry);
                sqlUnits.addAll(deleteSql);
                continue;
            }

            if (EventType.UPDATE.equalsIgnoreCase(eventType)) {
                List<SqlUnit> updateSql = this.updateSql(dataEntry);
                sqlUnits.addAll(updateSql);
                continue;
            }

            // ALTER CREATE TRUNCATE RENAME
            log.info("#47 eventType=" + eventType + ", dataEntry=" + dataMessage + ", pipeline=" + this.getPipeline());
        }

        return sqlUnits;
    }

    @SpringGraph
    protected List<SqlUnit> insertSql(DataEntry dataEntry) {
        List<SqlUnit> sqlUnits = new ArrayList<SqlUnit>();

        Integer rowCount = dataEntry.getRowCount();
        for (int rowIndex = 0; rowIndex < rowCount; rowIndex++) {
            SqlUnit insertRow = this.insertRow(dataEntry, rowIndex);
            sqlUnits.add(insertRow);
        }

        return sqlUnits;
    }

    @SpringGraph
    protected List<SqlUnit> deleteSql(DataEntry dataEntry) {
        List<SqlUnit> sqlUnits = new ArrayList<SqlUnit>();

        Integer rowCount = dataEntry.getRowCount();
        for (int rowIndex = 0; rowIndex < rowCount; rowIndex++) {
            SqlUnit deleteRow = this.deleteRow(dataEntry, rowIndex);
            sqlUnits.add(deleteRow);
        }

        return sqlUnits;
    }

    public List<SqlUnit> updateSql(DataEntry dataEntry) {
        List<SqlUnit> sqlUnits = new ArrayList<SqlUnit>();

        Integer rowCount = dataEntry.getRowCount();
        for (int rowIndex = 0; rowIndex < rowCount; rowIndex++) {
            SqlUnit updateRow = this.updateRow(dataEntry, rowIndex);
            sqlUnits.add(updateRow);
        }

        return sqlUnits;
    }

    /**
     * 拼装 insert 语句
     */
    public SqlUnit insertRow(DataEntry dataEntry, int rowIndex) {
        List<Object> parameters = new ArrayList<Object>();

        List<String> fields = new ArrayList<String>();
        List<String> values = new ArrayList<String>();

        Integer columnCount = dataEntry.getColumnCount(rowIndex);
        for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
            String columnName = dataEntry.getColumnName(rowIndex, columnIndex);
            fields.add("`" + columnName + "`");

            String columnValueAfter = dataEntry.getColumnValueAfter(rowIndex, columnIndex);
            if (null == columnValueAfter) {
                values.add("NULL");
            } else {
                values.add("?");
                parameters.add(columnValueAfter);
            }
        }

        String sql = "INSERT INTO " + PipelineUtil.targetSchemaTable(this.getPipeline(), dataEntry) + "(" + StringUtil.join(fields, ", ") + ") VALUES (" + StringUtil.join(values, ", ") + ") ";
        return new SqlUnit(sql, parameters.toArray());
    }

    public SqlUnit deleteRow(DataEntry dataEntry, int rowIndex) {
        List<Object> parameters = new ArrayList<Object>();
        List<String> where = this.where(dataEntry, rowIndex, parameters);

        String sql = "DELETE FROM " + PipelineUtil.targetSchemaTable(this.getPipeline(), dataEntry) + " WHERE " + StringUtil.join(where, " AND ");
        return new SqlUnit(sql, parameters.toArray());
    }

    public SqlUnit updateRow(DataEntry dataEntry, int rowIndex) {
        List<Object> parameters = new ArrayList<Object>();

        List<String> set = this.set(dataEntry, rowIndex, parameters);
        List<String> where = this.where(dataEntry, rowIndex, parameters);

        String sql = "UPDATE " + PipelineUtil.targetSchemaTable(this.getPipeline(), dataEntry) + " SET " + StringUtil.join(set, ", ") + " WHERE " + StringUtil.join(where, " AND ");
        return new SqlUnit(sql, parameters.toArray());
    }

    public List<String> set(DataEntry dataEntry, int rowIndex, List<Object> parameters) {
        List<String> set = new ArrayList<String>();

        Integer columnCount = dataEntry.getColumnCount(rowIndex);
        for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
            String columnName = dataEntry.getColumnName(rowIndex, columnIndex);
            String columnValueAfter = dataEntry.getColumnValueAfter(rowIndex, columnIndex);
            if (null == columnValueAfter) {
                set.add("`" + columnName + "`=NULL");
            } else {
                set.add("`" + columnName + "`=?");
                parameters.add(columnValueAfter);
            }
        }
        return set;
    }

    public List<String> where(DataEntry dataEntry, int rowIndex, List<Object> parameters) {
        List<String> where = new ArrayList<String>();
        boolean hasIsKeyColumn = DataEntryUtil.hasIsKeyColumn(dataEntry, rowIndex);
        Integer columnCount = dataEntry.getColumnCount(rowIndex);

        for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
            if (hasIsKeyColumn && !dataEntry.getColumnIsKey(rowIndex, columnIndex)) { // 如果有主键就主键作为条件,否则全部作为条件
                continue;
            }

            String columnName = dataEntry.getColumnName(rowIndex, columnIndex);
            String columnValueBefore = dataEntry.getColumnValueBefore(rowIndex, columnIndex);
            if (null == columnValueBefore) {
                where.add("`" + columnName + "` IS NULL");
            } else {
                where.add("`" + columnName + "`=?");
                parameters.add(columnValueBefore);
            }
        }

        return where;
    }
}