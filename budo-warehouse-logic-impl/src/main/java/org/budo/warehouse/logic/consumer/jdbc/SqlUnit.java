package org.budo.warehouse.logic.consumer.jdbc;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author limingwei
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SqlUnit {
    private String sql;

    private Object[] parameters;
}