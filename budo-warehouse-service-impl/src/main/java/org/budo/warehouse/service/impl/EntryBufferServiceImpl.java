package org.budo.warehouse.service.impl;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import org.budo.support.dao.page.Page;
import org.budo.warehouse.dao.api.IEntryBufferDao;
import org.budo.warehouse.service.api.IEntryBufferService;
import org.budo.warehouse.service.entity.EntryBuffer;
import org.springframework.stereotype.Service;

/**
 * @author limingwei
 */
@Service
public class EntryBufferServiceImpl implements IEntryBufferService {
    @Resource
    private IEntryBufferDao entryBufferDao;

    @Override
    public Integer insert(EntryBuffer entryBuffer) {
        return entryBufferDao.insert(entryBuffer);
    }

    @Override
    public Integer[] insertBatch(List<EntryBuffer> entryBuffers) {
        return entryBufferDao.insertBatch(entryBuffers);
    }

    @Override
    public Integer countNotFlushedByPipelineId(Integer pipelineId) {
        return entryBufferDao.countNotFlushedByPipelineId(pipelineId);
    }

    @Override
    public List<EntryBuffer> listNotFlushedByPipelineId(Integer pipelineId, Page page) {
        return entryBufferDao.listNotFlushedByPipelineId(pipelineId, page);
    }

    @Override
    public Timestamp findNotFlushedMaxCreatedAtByPipelineId(Integer pipelineId) {
        return entryBufferDao.findNotFlushedMaxCreatedAtByPipelineId(pipelineId);
    }

    @Override
    public Integer updateFlushedAtByIds(Timestamp flushedAt, List<Integer> ids) {
        return entryBufferDao.updateFlushedAtByIds(flushedAt,ids);
    }

    @Override
    public Timestamp findMaxCreatedAtByDataNodeId(Integer dataNodeId) {
        return entryBufferDao.findMaxCreatedAtByDataNodeId(dataNodeId);
    }

    @Override
    public Integer countByExample(EntryBuffer entryBuffer) {
        return entryBufferDao.countByExample(entryBuffer);
    }

    @Override
    public Integer deleteByFlushedAtLessThan(Timestamp flushedAt) {
        return entryBufferDao.deleteByFlushedAtLessThan(flushedAt);
    }
}