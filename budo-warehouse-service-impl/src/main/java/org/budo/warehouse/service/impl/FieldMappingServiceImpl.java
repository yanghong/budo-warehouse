package org.budo.warehouse.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.budo.ehcache.config.annotation.EhCacheConfig;
import org.budo.warehouse.dao.api.IFieldMappingDao;
import org.budo.warehouse.service.api.IFieldMappingService;
import org.budo.warehouse.service.entity.FieldMapping;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @author lmw
 */
@Service
public class FieldMappingServiceImpl implements IFieldMappingService {
    @Resource
    private IFieldMappingDao fieldMappingDao;

    @Override
    public List<FieldMapping> listByPipelineId(Integer pipelineId) {
        return fieldMappingDao.listByPipelineId(pipelineId);
    }

    @EhCacheConfig(timeToLiveSeconds = 60, timeToIdleSeconds = 30, maxElementsInMemory = 100, maxElementsOnDisk = 200, graph = true)
    @Cacheable("FieldMappingServiceListByPipelineIdCached")
    @Override
    public List<FieldMapping> listByPipelineIdCached(Integer pipelineId) {
        return fieldMappingDao.listByPipelineId(pipelineId);
    }

    @EhCacheConfig(timeToLiveSeconds = 60, timeToIdleSeconds = 30, maxElementsInMemory = 100, maxElementsOnDisk = 200, graph = true)
    @Cacheable("FieldMappingServiceFindOriginalFieldsValueByPipelineIdCached")
    @Override
    public Boolean findOriginalFieldsValueByPipelineIdCached(Integer pipelineId) {
        return fieldMappingDao.findOriginalFieldsValueByPipelineId(pipelineId);
    }

    @Override
    public List<Integer> listCountByPipelineIds(List<Integer> pipelineIds) {
        return fieldMappingDao.listCountByPipelineIds(pipelineIds);
    }
}