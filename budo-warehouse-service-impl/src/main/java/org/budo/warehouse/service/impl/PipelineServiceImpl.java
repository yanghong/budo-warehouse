package org.budo.warehouse.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.budo.ehcache.config.annotation.EhCacheConfig;
import org.budo.support.dao.page.Page;
import org.budo.support.dao.page.PageModel;
import org.budo.warehouse.dao.api.IPipelineDao;
import org.budo.warehouse.service.api.IPipelineService;
import org.budo.warehouse.service.entity.Pipeline;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @author limingwei
 */
@Service
public class PipelineServiceImpl implements IPipelineService {
    @Resource
    private IPipelineDao pipelineDao;

    @Override
    public List<Pipeline> listBySourceDataNode(Integer dataNodeId, Page page) {
        return pipelineDao.listBySourceDataNode(dataNodeId, page);
    }

    @EhCacheConfig(timeToLiveSeconds = 60, timeToIdleSeconds = 30, maxElementsInMemory = 100, maxElementsOnDisk = 200, graph = true)
    @Cacheable("PipelineServiceListBySourceDataNodeCached")
    @Override
    public List<Pipeline> listBySourceDataNodeCached(Integer dataNodeId, Page page) {
        if (null == dataNodeId) {
            return null;
        }

        return pipelineDao.listBySourceDataNode(dataNodeId, page);
    }

    @Override
    public PageModel<Pipeline> list(Page page) {
        return pipelineDao.list(page);
    }
}