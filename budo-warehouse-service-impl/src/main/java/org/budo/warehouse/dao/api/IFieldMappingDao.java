package org.budo.warehouse.dao.api;

import java.util.List;

import org.budo.warehouse.service.entity.FieldMapping;

/**
 * @author lmw
 */
public interface IFieldMappingDao {
    List<FieldMapping> listByPipelineId(Integer pipelineId);

    Boolean findOriginalFieldsValueByPipelineId(Integer pipelineId);

    List<Integer> listCountByPipelineIds(List<Integer> pipelineIds);
}