package org.budo.warehouse.dao.api;

import java.util.List;

import org.budo.support.dao.page.Page;
import org.budo.support.dao.page.PageModel;
import org.budo.warehouse.service.entity.Pipeline;

/**
 * @author limingwei
 */
public interface IPipelineDao {
    List<Pipeline> listBySourceDataNode(Integer dataNodeId, Page page);

    PageModel<Pipeline> list(Page page);
}