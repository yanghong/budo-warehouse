package org.budo.warehouse.dao.api;

import java.sql.Timestamp;
import java.util.List;

import org.budo.support.dao.page.Page;
import org.budo.warehouse.service.entity.EntryBuffer;

/**
 * @author limingwei
 */
public interface IEntryBufferDao {
    Integer countNotFlushedByPipelineId(Integer pipelineId);

    List<EntryBuffer> listNotFlushedByPipelineId(Integer pipelineId, Page page);

    Timestamp findNotFlushedMaxCreatedAtByPipelineId(Integer pipelineId);

    Integer updateFlushedAtByIds(Timestamp flushedAt, List<Integer> ids);

    Integer[] insertBatch(List<EntryBuffer> entryBuffers);

    Integer insert(EntryBuffer entryBuffer);

    Timestamp findMaxCreatedAtByDataNodeId(Integer dataNodeId);

    Integer countByExample(EntryBuffer entryBuffer);

    Integer deleteByFlushedAtLessThan(Timestamp flushedAt);
}