package org.budo.warehouse.dao.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.budo.mybatis.dao.MybatisDao;
import org.budo.support.dao.page.Page;
import org.budo.support.dao.page.PageModel;
import org.budo.support.lang.util.MapUtil;
import org.budo.warehouse.dao.api.IPipelineDao;
import org.budo.warehouse.service.entity.Pipeline;
import org.springframework.stereotype.Repository;

/**
 * @author limingwei
 */
@Repository
public class PipelineDaoImpl implements IPipelineDao {
    @Resource
    private MybatisDao mybatisDao;

    @Override
    public List<Pipeline> listBySourceDataNode(Integer dataNodeId, Page page) {
        String sql = "SELECT * FROM t_pipeline WHERE source_datanode_id=#{source_datanode_id} AND (deleted_at IS NULL OR deleted_at='') ";
        Map<String, Object> parameter = MapUtil.stringObjectMap("source_datanode_id", dataNodeId);
        return mybatisDao.listBySql(Pipeline.class, sql, parameter, page);
    }

    @Override
    public PageModel<Pipeline> list(Page page) {
        List<Pipeline> list = mybatisDao.list(Pipeline.class, page);
        return new PageModel<Pipeline>(list, page);
    }
}