package org.budo.warehouse.service.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 描述属性对应关系
 * 
 * @author lmw
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_field_mapping")
public class FieldMapping implements Serializable {
    private static final long serialVersionUID = 3128562667490454427L;

    public static final String PREFIX = "$_";

    public static final String ORIGINAL_FIELDS = PREFIX + "original_fields";

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "pipeline_id")
    private Integer pipelineId;

    /**
     * 目标列名
     */
    @Column(name = "field_name")
    private String fieldName;

    /**
     * 目标列的值的表达式
     */
    @Column(name = "field_value")
    private String fieldValue;

    /**
     * 是否已删除，未删除时为空
     */
    @Column(name = "deleted_at")
    private Timestamp deletedAt;
}