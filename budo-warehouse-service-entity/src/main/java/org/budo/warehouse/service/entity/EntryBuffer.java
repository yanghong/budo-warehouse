package org.budo.warehouse.service.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 为了控制邮件发送频率,将相关内容写入数据库,适当时机再发送
 * 
 * @author limingwei
 */
@Getter
@Setter
@Accessors(chain = true)
@AllArgsConstructor
@ToString
@Table(name = "t_entry_buffer")
public class EntryBuffer implements Serializable {
    private static final long serialVersionUID = 7011920206255705181L;

    public EntryBuffer() {
        this.setCreatedAt(new Timestamp(System.currentTimeMillis()));
    }

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "pipeline_id")
    private Integer pipelineId;

    @Column(name = "event_type")
    private String eventType;

    @Column(name = "schema_name")
    private String schemaName;

    @Column(name = "table_name")
    private String tableName;

    @Column(name = "`sql`")
    private String sql;

    @Column
    private String rows;

    @Column(name = "created_at")
    private Timestamp createdAt;

    @Column(name = "flushed_at")
    private Timestamp flushedAt;
}