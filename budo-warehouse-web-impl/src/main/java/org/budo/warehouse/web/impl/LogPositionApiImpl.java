package org.budo.warehouse.web.impl;

import org.budo.warehouse.web.api.LogPositionApi;
import org.springframework.stereotype.Component;

/**
 * @author lmw
 */
@Component
public class LogPositionApiImpl implements LogPositionApi {}