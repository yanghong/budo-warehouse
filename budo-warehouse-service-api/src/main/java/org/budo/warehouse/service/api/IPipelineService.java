package org.budo.warehouse.service.api;

import java.util.List;

import org.budo.support.dao.page.Page;
import org.budo.support.dao.page.PageModel;
import org.budo.warehouse.service.entity.Pipeline;

/**
 * @author limingwei
 */
public interface IPipelineService {
    List<Pipeline> listBySourceDataNode(Integer dataNodeId, Page page);

    List<Pipeline> listBySourceDataNodeCached(Integer dataNodeId, Page page);

    PageModel<Pipeline> list(Page page);
}