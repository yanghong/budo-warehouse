package org.budo.warehouse.service.api;

import java.util.List;

import org.budo.support.dao.page.Page;
import org.budo.support.dao.page.PageModel;
import org.budo.warehouse.service.entity.DataNode;

/**
 * @author limingwei
 */
public interface IDataNodeService {
    List<DataNode> listSourceDataNodes(Page page);

    DataNode findById(Integer id);

    DataNode findByIdCached(Integer id);

    List<DataNode> listMysqlNodes(Page page);

    PageModel<DataNode> list(Page page);

    List<String> listNameByIds(List<Integer> ids);
}