package org.budo.warehouse.service.api;

import java.util.List;

import org.budo.warehouse.service.entity.FieldMapping;

/**
 * @author lmw
 */
public interface IFieldMappingService {
    List<FieldMapping> listByPipelineId(Integer pipelineId);

    List<FieldMapping> listByPipelineIdCached(Integer pipelineId);

    Boolean findOriginalFieldsValueByPipelineIdCached(Integer pipelineId);

    List<Integer> listCountByPipelineIds(List<Integer> pipelineIds);
}