package org.budo.warehouse.logic.api;

/**
 * @author limingwei
 */
public interface DataEntry {
    interface EventType {
        String INSERT = "INSERT", //
                DELETE = "DELETE", //
                UPDATE = "UPDATE";
    }

    String getEventType();

    /**
     * DDL类型事件,返回操作语句
     */
    String getSql();

    String getSchemaName();

    String getTableName();

    Long getExecuteTime();

    /**
     * 行数
     */
    Integer getRowCount();

    /**
     * 某行的列数
     */
    Integer getColumnCount(Integer rowIndex);

    /**
     * 列名
     */
    String getColumnName(Integer rowIndex, Integer columnIndex);

    /**
     * 列是否主键
     */
    Boolean getColumnIsKey(Integer rowIndex, Integer columnIndex);

    /**
     * 变更前的值
     */
    String getColumnValueBefore(Integer rowIndex, Integer columnIndex);

    /**
     * 变更后的值
     */
    String getColumnValueAfter(Integer rowIndex, Integer columnIndex);
}