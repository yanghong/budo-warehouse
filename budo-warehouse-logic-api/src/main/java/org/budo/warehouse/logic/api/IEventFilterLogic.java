package org.budo.warehouse.logic.api;

import org.budo.warehouse.service.entity.Pipeline;

/**
 * @author lmw
 */
public interface IEventFilterLogic {
    /**
     * @see com.alibaba.otter.canal.filter.CanalEventFilter
     */
    DataMessage filter(Pipeline pipeline, DataMessage dataMessage);
}