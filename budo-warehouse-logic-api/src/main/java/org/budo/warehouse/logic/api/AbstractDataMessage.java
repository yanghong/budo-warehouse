package org.budo.warehouse.logic.api;

import java.io.Serializable;
import java.util.List;

/**
 * @author lmw
 */
public abstract class AbstractDataMessage implements DataMessage, Serializable {
    private static final long serialVersionUID = -3737347541093206991L;

    @Override
    public Integer getRowCount() {
        List<DataEntry> dataEntries = this.getDataEntries();
        if (null == dataEntries) {
            return null;
        }

        Integer _rowCount = 0;
        for (DataEntry dataEntry : dataEntries) {
            Integer rowCount = dataEntry.getRowCount();
            _rowCount += rowCount;
        }
        return _rowCount;
    }
}
