package org.budo.warehouse.logic.api;

import java.util.List;

/**
 * @author lmw
 */
public interface DataMessage {
    /**
     * 消息ID
     */
    Long getId();

    /**
     * 来源数据节点ID
     */
    Integer getDataNodeId();

    /**
     * 内容
     */
    List<DataEntry> getDataEntries();

    /**
     * 所有DataEntry的行数的和
     */
    Integer getRowCount();
}