package org.budo.warehouse.logic.api;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author lmw
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DataMessagePojo extends AbstractDataMessage {
    private static final long serialVersionUID = -3590590864686418125L;

    private Long id;

    private Integer dataNodeId;

    private List<DataEntry> dataEntries;
}