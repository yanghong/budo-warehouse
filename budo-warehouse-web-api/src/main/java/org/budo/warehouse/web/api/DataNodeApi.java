package org.budo.warehouse.web.api;

import java.util.List;

import org.budo.dubbo.protocol.http.authentication.AuthenticationCheck;
import org.budo.support.dao.page.Page;
import org.budo.support.dao.page.PageModel;
import org.budo.warehouse.service.entity.DataNode;

/**
 * @author limingwei
 */
@AuthenticationCheck
public interface DataNodeApi {
    PageModel<DataNode> list(Page page);

    List<String> listNameByIds(List<Integer> ids);
}