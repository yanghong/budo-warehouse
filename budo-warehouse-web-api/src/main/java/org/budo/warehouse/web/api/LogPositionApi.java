package org.budo.warehouse.web.api;

import org.budo.dubbo.protocol.http.authentication.AuthenticationCheck;

/**
 * @author lmw
 */
@AuthenticationCheck
public interface LogPositionApi {}