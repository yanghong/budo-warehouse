package org.budo.warehouse.web.api;

import java.util.List;

import org.budo.dubbo.protocol.http.authentication.AuthenticationCheck;
import org.budo.support.dao.page.Page;
import org.budo.support.dao.page.PageModel;
import org.budo.warehouse.service.entity.FieldMapping;

/**
 * @author limingwei
 */
@AuthenticationCheck
public interface FieldMappingApi {
    PageModel<FieldMapping> listByPipelineId(Integer pipelineId, Page page);

    List<Integer> listCountByPipelineIds(List<Integer> pipelineIds);
}